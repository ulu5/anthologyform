# README #

- Quick test to get setup using Umbraco with .NET and SQL server database. 
- Uses NLog to log to C:\logs\AnthologyErrors.log
- Uses SQL server to store visitor's information

### What is this repository for? ###

* Just a quick and dirty test for Anthology Marketing Team
* Version 0.1

### How do I get set up? ###

* Create Users database by restoring from backup in ~/DbBackup/Users.bak
* Run using IIS or IIS express
* Change Web.config connection string to use the connection the database created in step #1

### Contribution guidelines ###

* N/A - Just a sandbox playground for Umbraco, nothing to contribute to here. :( Feel free to fork.

### Who do I talk to? ###

* Andrew Taeoalii