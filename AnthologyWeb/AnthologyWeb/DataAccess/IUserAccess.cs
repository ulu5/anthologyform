﻿using System.Collections.Generic;
using AnthologyWeb.Models;
using AnthologyWeb.Models.DbModels;


namespace AnthologyWeb.DataAccess
{
    public interface IUserAccess
    {
        bool SignIn(SignInModel signInModel);

        List<User> GetSignedInPeople();
    }
}
