﻿
using System.Data.Entity;
using AnthologyWeb.Models.DbModels;

namespace AnthologyWeb.DataAccess
{
    public class UsersContext : DbContext
    {
        public UsersContext() : base("UsersContext")
        {
        }

        public DbSet<User> SignedInUsers { get; set; }
    }
}
