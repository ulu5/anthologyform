﻿using System;
using System.Collections.Generic;
using AnthologyWeb.Models;
using AnthologyWeb.Models.DbModels;
using System.Linq;
using NLog;

namespace AnthologyWeb.DataAccess
{
    public class UserAccess : IUserAccess
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Sign in.
        /// </summary>
        /// <param name="signInModel"></param>
        /// <returns></returns>
        public bool SignIn(SignInModel signInModel)
        {
            try
            {
                using (var dbContext = new UsersContext())
                {
                    dbContext.SignedInUsers.Add(new User
                    {
                        CreatedDate = DateTime.Now,
                        Email = signInModel.Email,
                        FirstName = signInModel.FirstName,
                        LastName = signInModel.LastName
                    });
                    dbContext.SaveChanges();
                    return true;
                }
            }
            catch(Exception e)
            {
                logger.Error(e, "Failed to create user");
            }
            return false;
        }

        /// <summary>
        /// Get the list of all people who signed in.
        /// </summary>
        /// <returns></returns>
        public List<User> GetSignedInPeople()
        {
            using (var dbContext = new UsersContext())
            {
                return dbContext.SignedInUsers.ToList();
            }
        }
    }
}
