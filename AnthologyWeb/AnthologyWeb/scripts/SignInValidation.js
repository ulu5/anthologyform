﻿
var EMAIL_REGEX = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$/;
var NAME_REGEX = /^[A-Za-z]+-?[A-Za-z]*.?$/;

// Validate email with regex.
function ValidateEmail()
{
    var email = document.getElementById("Email").value;
    var passedTest = EMAIL_REGEX.test(email);
    var errorField = document.getElementById("emailError");
    errorField.hidden = passedTest;
    return passedTest;
}

// Validate First Name.
function ValidateFirstName()
{
    var firstName = document.getElementById("FirstName").value;
    return ValidateName(firstName, "firstNameError");
}

// Validate Last Name.
function ValidateLastName()
{
    var lastName = document.getElementById("LastName").value;
    return ValidateName(lastName, "lastNameError");
}

// Validate name with regex.
function ValidateName(name, nameErrorId)
{
    var passedTest = NAME_REGEX.test(name);
    var errorField = document.getElementById(nameErrorId);
    errorField.hidden = passedTest;
    return passedTest;
}

// Validate all fields before submission.
function HandleSumbit(event)
{
    event.preventDefault();

    // check everything 
    return ValidateEmail() && ValidateFirstName() && ValidateLastName();
}
