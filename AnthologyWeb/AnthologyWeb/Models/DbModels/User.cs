﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AnthologyWeb.Models.DbModels
{
    public class User
    {
        [Key]
        public int Id { get; set; }

        public DateTime CreatedDate { get; set; }

        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}
