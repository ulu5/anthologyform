﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AnthologyWeb.Models
{
    public class SignInModel
    {
        [Required]
        [RegularExpression(@"^[A-Za-z]+-?[A-Za-z]*.?$")]
        public string FirstName { get; set; }

        [Required]
        [RegularExpression(@"^[A-Za-z]+-?[A-Za-z]*.?$")]
        public string LastName { get; set; }

        [Required]
        [RegularExpression(@"^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$")]
        public string Email { get; set; }
    }
}