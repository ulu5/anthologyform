﻿using System;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Web.Mvc;
using AnthologyWeb.Models;
using AnthologyWeb.DataAccess;


namespace AnthologyWeb.Controllers
{
    public class SignInController : SurfaceController
    {
       
        private static readonly Dictionary<string, string> MODEL_ERRORS = new Dictionary<string, string>
        {
            { "FirstName", "Invalid first name. Must only use letters A-Z, period(.), or hyphen(-)."},
            { "LastName", "Invalid last name. Must use only letters A-Z, period(.), or hyphen(-)."},
            { "Email", "Invalid email address"}
        };

        private static readonly string SIGN_IN_MODEL_SESSION = "SignInModel";

        private IUserAccess _userAccess;


        public IUserAccess UserAccess
        {
            get
            {
                return _userAccess ?? (_userAccess = new UserAccess());
            }

            set
            {
                _userAccess = value;
            }
        }

        /// <summary>
        /// Get method.
        /// </summary>
        /// <returns></returns>
        public ActionResult SignIn()
        {
            var signInModel = Session[SIGN_IN_MODEL_SESSION] as SignInModel;

            if(signInModel == null)
            {
                signInModel = new SignInModel();
            }

            Session.Clear();

            return PartialView("SignInForm", signInModel);
        }

        /// <summary>
        /// Post method.
        /// </summary>
        /// <param name="signInModel"></param>
        /// <returns></returns>
        public ActionResult HandlePost(SignInModel signInModel)
        {
            // TODO: validation
            if (ModelState.IsValid)
            {
                // put into db
                if(UserAccess.SignIn(signInModel))
                {
                    return new RedirectResult("VisitorLog");
                }
                else
                {
                    ModelState.AddModelError("FirstName", new Exception("Internal Processing Error"));
                    Session[SIGN_IN_MODEL_SESSION] = signInModel;
                    return new RedirectResult("SignIn");
                }
                
            }
            else
            {
                var errors = new List<string>();
                // get all the model errors
                foreach(var modelError in MODEL_ERRORS)
                {
                    var errorExists = ViewData.ModelState.Where(modelState => modelState.Key == modelError.Key && modelState.Value.Errors.Any()).Any();
                    if(errorExists)
                    {
                        errors.Add(modelError.Value);
                    }
                }

                Session[SIGN_IN_MODEL_SESSION] = signInModel;

                // return to the same view with the errors
                return new RedirectResult("SignIn");
            }
        }

        /// <summary>
        /// Show the view with all the visitors in the database.
        /// </summary>
        /// <returns></returns>
        public ActionResult VisitorLog()
        {
            var visitorLog = UserAccess.GetSignedInPeople();
            return PartialView("VisitorLogTable", visitorLog);
        }

    }
}